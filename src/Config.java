/*
 * Class: Config
 * 
 * Simple configuration file definition class
 * 
 */
public class Config {
	  private String cfg; 
	  private String Main;
	  private String Package;
	  private String Project;
	  private String Args;
	  
	  public String getCfg() {
	    return cfg;
	  }
	  public void setProject(String in){
		  this.Project = in;
	  }
	  public String getProject(){
		  return Project;
	  }
	  public void setCfg(String in) {
	    this.cfg = in;
	  }
	  public String getMain() {
	    return Main;
	  }
	  public void setMain(String in){
		  this.Main = in;
	  }
	  public String getPackage(){
		  return Package;
	  }
	  public void setPackage(String in){
		  this.Package = in;
	  }
	  public String getArgs(){
		  return Args;
	  }
	  public void setArgs(String args){
		  this.Args = args;
	  }
	} 