/*
 * Class: SaveConfiguration
 * 
 * Class to write the new Run Configuration to the run.cfg xml file
 * 
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.StringWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;


public class SaveConfiguration {
    public static void saveConfig(String configName, String proj, String main, String pkg, String args, String directory, String projectName) throws Exception {
        new StringWriter();
        XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
        new File(directory+"/run.cfg");
        XMLStreamWriter stream = outputFactory.createXMLStreamWriter(new FileOutputStream(directory+"/run.cfg"), "UTF-8");
        
        stream.writeStartDocument("UTF-8", "1.0");
        stream.writeCharacters("\n");
        stream.writeStartElement("RunConfig");
        stream.writeCharacters("\n\t");
        stream.writeStartElement("item");
        stream.writeAttribute("config", configName);
        
		stream.writeCharacters("\n\t\t");
        stream.writeStartElement("Project");
        stream.writeCharacters(projectName);
        stream.writeEndElement();
        
        stream.writeCharacters("\n\t\t");
        stream.writeStartElement("Main");
        stream.writeCharacters(main);
        stream.writeEndElement();
        
        stream.writeCharacters("\n\t\t");
        stream.writeStartElement("Package");
        stream.writeCharacters(pkg);
        stream.writeEndElement();
        
        stream.writeCharacters("\n\t\t");
        stream.writeStartElement("Args");
        stream.writeCharacters(" ");
        stream.writeEndElement();
        
        stream.writeCharacters("\n\t");
        stream.writeEndElement();
        stream.writeCharacters("\n");
        stream.writeEndElement();
        stream.writeEndDocument();
        
        stream.flush();

      }
}
