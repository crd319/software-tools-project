/*
 * Class: MyDiagnosticListener
 * 
 * Class to invoke the Java Diagonstic Listener
 * 
 */

import java.util.Locale;

import javax.swing.JTextArea;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaFileObject;


public class MyDiagnosticListener implements DiagnosticListener<JavaFileObject>	{
        
		private final static String newline = "\n";
		private JTextArea compilerPane;
		
		public MyDiagnosticListener(JTextArea compilerPane){
			this.compilerPane = compilerPane;
		}
	
		//Append any error messages to the compiler pane
    	public void report(Diagnostic<? extends JavaFileObject> diagnostic)	{
    		compilerPane.append("Source: " + diagnostic.getSource()+newline);
    		compilerPane.append("Error at line: " + diagnostic.getLineNumber()+newline);
    		compilerPane.append(diagnostic.getMessage(Locale.ENGLISH)+newline);
    		compilerPane.append(" ");
        }

    }
