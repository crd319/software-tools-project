/*
 * Class: Compiler
 * 
 * Class to invoke the Java compiler from within the IDE
 * and retreive standard error messages from the Diagonstic Listener
 * 
 */

import java.io.File;
import java.util.Arrays;

import javax.swing.JTextArea;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;


public class Compiler {
	public static boolean compile(File[] javaFiles, JTextArea compilerPane, String classOutputFolder)
    {
        //get system compiler:
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

        MyDiagnosticListener c = new MyDiagnosticListener(compilerPane);
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(c, null, null);

        Iterable<? extends JavaFileObject> fileObjects;
        fileObjects = fileManager.getJavaFileObjects(javaFiles);
        
        //specify classes output folder
        Iterable<String> options = Arrays.asList("-d", classOutputFolder);
        JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager,c, options, null, fileObjects);
        Boolean result = task.call();
        if (result == true)
        {
        	compilerPane.append("Compilation Successful!"+"\n");
        	return true;
        }
        else{
        	return false;
        }
    
     }
}