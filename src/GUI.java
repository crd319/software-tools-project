/**
 * Class: GUI
 * 
 * Define the Graphical User Interface and ActionEvents for button prompts
*/

import java.awt.*;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileSystemView;

import org.fife.ui.rtextarea.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.text.Document;
import javax.swing.tree.DefaultTreeCellRenderer;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;

import java.awt.EventQueue;

import org.fife.ui.autocomplete.*; 
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

import javax.swing.JTabbedPane;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;


public class GUI extends JFrame{
	/**
	 * Define static variables to be used throughout the GUI class
	*/
	private static final long serialVersionUID = 1L;
	public final RSyntaxTextArea codeEditor = new RSyntaxTextArea();
	public JFrame findFrame = new JFrame();

	private JTextField pName;
	private JTextField fName;
	private JTextArea arguments;
	private JButton build;
	private JButton cancel;
	private JTextField configName;
	private JTextField replaceWith;
	private static String text;
	
	public JDialog runConfiguration = new JDialog();
	public JDialog newProj = new JDialog();
	
	private JTextField searchField;
	
    private static String directory = "";
	private static String classOutputFolder = directory+"\bin";

	private static String projectName = "";
	private JTree tree;    
    private JLabel status;
    BufferedWriter os;
    
    public static JTextArea compilerPane = new JTextArea();
	final JTextArea consolePane = new JTextArea();

    private JScrollPane treeFrame;
    
    final GridBagConstraints gbc_panel = null;
    
    private JPanel contentPane;

	/**
	 * Create the frame.
	 */
    public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 743, 411);
		FileSystemView.getFileSystemView();
		
		codeEditor.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
	    codeEditor.setCodeFoldingEnabled(true);
	    codeEditor.setAntiAliasingEnabled(true);
	    codeEditor.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        consolePane.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        
	    RTextScrollPane scrPane = new RTextScrollPane(codeEditor);
	    scrPane.setFoldIndicatorEnabled(true);
		treeFrame = new JScrollPane();
		
		 CompletionProvider provider = Completion.createCompletionProvider();
	     AutoCompletion ac = new AutoCompletion(provider);
	     ac.install(codeEditor);

		JFileChooser fileChooser = new JFileChooser();
		
		//Create a menu bar for the GUI
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		// Add a File Menu option
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
		JMenuItem newButton = new JMenuItem("New",'N');
		fileMenu.add(newButton);
		newButton.setMnemonic('N');
		newButton.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		
		//Add a Edit Menu option
		JMenu editMenu = new JMenu("Edit");
		menuBar.add(editMenu);
		JMenuItem findButton = new JMenuItem("Find");
		editMenu.add(findButton);
		findButton.setMnemonic('F');
		findButton.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK));
		//Display the Find/Replace window when Find menu item is selected
		findButton.addActionListener(new ActionListener()	{
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						findFrame.setVisible(true);
					}
				});
			}
		});

		//Make a New file button
		newButton.addActionListener(new ActionListener()	{
			public void actionPerformed(ActionEvent arg0) {
				
				//If a project is active
				if (!directory.equals("")){
					JTextField fn = new JTextField(10);
					JTextField pkgn = new JTextField(10);
					JPanel myPanel = new JPanel();
				    myPanel.add(new JLabel("Filename:"));
				    myPanel.add(fn);
				    myPanel.add(new JLabel("Package:"));
				    myPanel.add(pkgn);
					int result = JOptionPane.showConfirmDialog(null, myPanel, "New File", JOptionPane.OK_CANCEL_OPTION);	
					
					 if (result == JOptionPane.OK_OPTION) {
						 File tempDIR = new File(directory+"/src/"+pkgn.getText());
						 if((!tempDIR.exists()) && (!(pkgn.getText().equals("default")))){
							 tempDIR.mkdir();
						 }
						 //Make a new file and define the base text
						 File f = new File(directory+"/src/"+pkgn.getText()+"/"+fn.getText());
						 try{
							String newFile = "package "+ pkgn.getText()+";\n"
										+"\n"
										+"class "+(fn.getText().substring(0,fn.getText().length()-5))+"{\n"
										+"\n"
										+"}";
							f.createNewFile();
							BufferedWriter out = new BufferedWriter(new FileWriter(f));
							out.write(newFile);
							out.close();
							FileReader reader = new FileReader( f );
							BufferedReader br = new BufferedReader(reader);
							codeEditor.read( br, null );
							br.close();
							//Refresh the tree
							makeTree();
						}catch(IOException e){}
					 }
				}else{
					JOptionPane.showMessageDialog(null,"Project not found", 
	                		"Project Error", JOptionPane.PLAIN_MESSAGE);
				}
		        makeTree();
			}
			
		});
		
		//Open a file
		JMenuItem openButton = new JMenuItem("Open",'O');
		fileMenu.add(openButton);
		openButton.setMnemonic('O');
		openButton.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		
		openButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser fc = new JFileChooser();
				fc.setCurrentDirectory(new File(System.getProperty("user.dir")));	
				try	{
					int returnVal = fc.showOpenDialog(GUI.this);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						
				        File file = fc.getSelectedFile();
				        FileReader reader = new FileReader( file );
                        BufferedReader br = new BufferedReader(reader);
                        codeEditor.read( br, null );
                        br.close();
                        
                       codeEditor.requestFocus();
                }
             }
                catch(Exception e2) { JOptionPane.showMessageDialog(null,"File not Found", 
                		"s", JOptionPane.PLAIN_MESSAGE);  }
			}
		});

		JMenuItem mntmSave = new JMenuItem("Save",'S');
		mntmSave.setMnemonic('S');
		mntmSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
	     
		//Save a file
		mntmSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try
                {
					String getFile = status.getText();
					File name = new File(getFile);
						if(name.exists())	{
							//Prompt the user of the save, preventing saving unwanted edits
							int n = JOptionPane.showConfirmDialog(
		                            null, "File already exists.  Do you wish to save over it?");
		                    if (n == JOptionPane.YES_OPTION) {
		                    	
		                    	BufferedWriter writer = new BufferedWriter(new FileWriter(name));
		                    	String t = codeEditor.getText();
								writer.write(t);
								writer.close();
								
								makeTree();
		                    }
						}
						else	{
							try	{
								BufferedWriter writer = new BufferedWriter(new FileWriter(name));
		                    	String t = codeEditor.getText();
								writer.write(t);
								writer.close();
								makeTree();
							}catch(Exception e){}
						}

					}
                catch(Exception ex22)	{}
               }
		});
		fileMenu.add(mntmSave);
		
		//Project Menu items
		JMenu mnProject = new JMenu("Project");
		menuBar.add(mnProject);
		
		//Make a new Project
		JMenuItem mntmNew = new JMenuItem("New");
		mntmNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String inputFilename;
				
				//Get the new project name
				inputFilename = JOptionPane.showInputDialog("Project Name: " );
				projectName = inputFilename;
				if (inputFilename !=null){
					File newDIR = new File(inputFilename);
					//Check that the project doesn't already exist.
					if(!newDIR.exists()){		
						newDIR.mkdir();
						directory = newDIR.getAbsolutePath();
						//Make a source file directory
						File sourceDIR = new File(directory+"/src");
						sourceDIR.mkdir();
						//make a class file directory
						File classDIR = new File(directory+"/bin");	
						classDIR.mkdir();
						//Get a Main class filename from the user
						//Filename MUST include .java
						String mainClass = JOptionPane.showInputDialog("Main fileName: " );
						if(mainClass !=null){
							//Create a package for the main class
							String mainPkg = JOptionPane.showInputDialog("Main Package: " );
								if (mainPkg != null && (!(mainPkg.equals("default")))){
								File pkgDIR = new File(directory+"/src/"+mainPkg);
								pkgDIR.mkdir();
								File main = new File(directory+"/src/"+mainPkg+"/"+mainClass);
								//Make a Main class java file.
								try {
									String newFile = "package "+ mainPkg+";\n"
											+"\n"
											+"class "+(mainClass.substring(0,mainClass.length()-5))+"{\n"
											+"	public static void main(String[] args){\n"
											+"	}\n"
											+"\n"
											+"}";
									main.createNewFile();
									BufferedWriter out = new BufferedWriter(new FileWriter(main));
									out.write(newFile);
									out.close();
								//Catch exceptions
								} catch (IOException e) {
									
									e.printStackTrace();
								}
								//Make the default Run Configuration File
								try {
									SaveConfiguration.saveConfig("Default",inputFilename, (mainClass.substring(0,mainClass.length()-5)), mainPkg, " ", directory, projectName);
								} catch (Exception e) {
									
									e.printStackTrace();
								}
								makeTree();
							}
						}
					}
					else  {
						JOptionPane.showMessageDialog(null, "Project already exists" );
						
					}
				}
			}
		});

		mnProject.add(mntmNew);
		treeFrame.setViewportView(tree);
		
		//Load a pre-existing project
		JMenuItem mntmLoad = new JMenuItem("Load");
		
		mntmLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					JFileChooser fileChooser = new JFileChooser();
			        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			        fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
			        int returnVal = fileChooser.showOpenDialog(GUI.this);
			       
			        if (returnVal == JFileChooser.APPROVE_OPTION){
			        	directory = fileChooser.getSelectedFile().getAbsolutePath();
			        	projectName = directory.substring(directory.lastIndexOf("\\")+1,directory.length());
			        	System.out.println(projectName);
			        	makeTree();
			        }
			        validate();
		        	repaint();
			     }
			});
		mnProject.add(mntmLoad);
		
		JMenu mnRun = new JMenu("Run");
		menuBar.add(mnRun);
		
		//Compile a project
		JMenuItem mntmBuild = new JMenuItem("Build");
		mnRun.add(mntmBuild);
			mntmBuild.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0){
					compilerPane.setText("");
					//Get all files in the project
					File curDIR = new File(directory+"/src/");
					File[] listOfDIR = curDIR.listFiles();
					ArrayList<File> listOfFiles = new ArrayList<File>();
					for(int i = 0; i< listOfDIR.length;i++ ){
						if(listOfDIR[i].isFile()){
							listOfFiles.add(listOfDIR[i]);
						}else if(listOfDIR[i].isDirectory()){
							File[] subDIR = listOfDIR[i].listFiles();
							for(int j = 0; j<subDIR.length;j++){
								listOfFiles.add(subDIR[j]);
							}
						}
					}
					File[] allFiles = null;
					allFiles = listOfFiles.toArray(new File[listOfFiles.size()]);
					
					//Call the compiler function
					classOutputFolder = directory+"/bin";
					Compiler.compile(allFiles,compilerPane,classOutputFolder);
					makeTree();
				}
			});
			
			//Make a new package
			JMenuItem packages = new JMenuItem("Create Package");
			fileMenu.add(packages);
			packages.addActionListener(new ActionListener()	{
				public void actionPerformed(ActionEvent arg0) {
					
					String packageName = JOptionPane.showInputDialog("Package Name: " );
					
					File main = new File(directory+"/src/"+packageName);
					File newDIR = new File(packageName);

					File main1 = new File(directory+"/bin/"+packageName);

					if( (!newDIR.exists()) && (!(packageName.equals("default"))) ){
						try {
							main.mkdir();
							main1.mkdir();	
						} catch (Exception e) {			
							e.printStackTrace();
						}
							makeTree();
						}
					
					else	{
						JOptionPane.showInputDialog("Package already exists: " );
						
					}
			}
			});
		
		//Execute the compiled project
		JMenuItem mntmRun = new JMenuItem("Run");
		mnRun.add(mntmRun);
		mntmRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				consolePane.setText("");
				ArrayList<Config> configs = new ArrayList<Config>();
				ArrayList<String> names = new ArrayList<String>();
				//Read in all run configurations from run.cfg and display options
				//of run configurations to the user.
				try{
					try	{
						XMLInputFactory inputXML = XMLInputFactory.newInstance();
						InputStream input = new FileInputStream(directory+"/run.cfg");
						XMLEventReader eventReader = inputXML.createXMLEventReader(input);
						Config config = null;
						
						while (eventReader.hasNext()) {
					        XMLEvent event = eventReader.nextEvent();

					        if (event.isStartElement()) {
					          StartElement startElement = event.asStartElement();
					          // If we have an item element, we create a new item
					          if (startElement.getName().getLocalPart() == ("item")) {
					            config = new Config();
					            // We read the attributes from this tag and add the date
					            // attribute to our object
					            @SuppressWarnings("unchecked")
								Iterator<Attribute> attributes = startElement.getAttributes();
					            while (attributes.hasNext()) {
					              Attribute attribute = attributes.next();
					              if (attribute.getName().toString().equals("config")) {
					                config.setCfg(attribute.getValue());
					                names.add(attribute.getValue());
					              }

					            }
					          }

					          if (event.isStartElement()) {
					            if (event.asStartElement().getName().getLocalPart()
					                .equals("Main")) {
					              event = eventReader.nextEvent();
					              config.setMain(event.asCharacters().getData());
					              continue;
					            }
					          }
					          if (event.isStartElement()) {
						            if (event.asStartElement().getName().getLocalPart()
						                .equals("Project")) {
						              event = eventReader.nextEvent();
						              config.setProject(event.asCharacters().getData());
						              continue;
						            }
						          }
					          if (event.asStartElement().getName().getLocalPart()
					              .equals("Package")) {
					            event = eventReader.nextEvent();
					            config.setPackage(event.asCharacters().getData());
					            continue;
					          }

					          if (event.asStartElement().getName().getLocalPart()
					              .equals("Args")) {
					        	event = eventReader.nextEvent();
					            XMLEvent e2 = event;
					            if(e2.isEndElement()){
					            	config.setArgs(" ");
					            }else{
					            	config.setArgs(event.asCharacters().getData());
					            }
					            continue;
					          }
					        }
					        // If we reach the end of an item element, we add it to the list
					        if (event.isEndElement()) {
					          EndElement endElement = event.asEndElement();
					          if (endElement.getName().getLocalPart() == ("item")) {
					            configs.add(config);
					          }
					        }

					      }
					}catch (FileNotFoundException e) {
					      e.printStackTrace();
				    } catch (XMLStreamException e) {
				      e.printStackTrace();
				    }
						
					Object[] possibilities = names.toArray();
					String selectedCfg = (String)JOptionPane.showInputDialog(
		                    null,
		                    "Select Run Configuration: ",
		                    "Customized Dialog",
		                    JOptionPane.PLAIN_MESSAGE,
		                    null,
		                    possibilities,
		                    possibilities[0]);
							
					Config get = null;
					for (Config c : configs){
						if(c.getCfg().equals(selectedCfg)){
							get = c;
						}
					}
					
					final String pkg = get.getPackage();
					final String main = get.getMain();
					final String proj = get.getProject();
					final String args = get.getArgs();
					
					//Create a runtime process to execute java
					final Process p = Runtime.getRuntime().exec("java -cp "+proj+"/bin/ "+pkg+"."+main+" "+args);
					
					//Create a new thread to handle input, output, error streams from executed process.
					//This separates the threads allowing the user to access the GUI while a process
					//Is being executed.
					Thread one = new Thread(){
						public void run(){
							try{
								//
								BufferedReader is = new BufferedReader(new InputStreamReader(p.getInputStream())); // is the created Process instance
								BufferedReader err = new BufferedReader(new InputStreamReader(p.getErrorStream()));
								os = new BufferedWriter(new OutputStreamWriter(p.getOutputStream())); // is a BufferedWriter instance defined outside, it will be used for input redirection.
								
								String line = null;
								while ((line = is.readLine()) != null) {
									consolePane.append(line+"\n");
								}
								while ((line = err.readLine()) != null) {
									consolePane.append(line+"\n");
								}
								
								
							}catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					};

					one.start();
					
				}catch(IOException e){}
			}
		});
		
		//Add key listeners for the console pane and connect those to the BufferedWriter for the process
		consolePane.addKeyListener(new KeyAdapter() {
			   public void keyTyped(KeyEvent e) {
			    //Send the typed key value to the BufferedWriter
				   int id = e.getID();
				   if(id == KeyEvent.KEY_TYPED){
					   try {
						os.write(e.getKeyChar());
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				   }
				}
			   public void keyReleased(KeyEvent a2){
				   int key = a2.getKeyCode();
				   if (key == KeyEvent.VK_ENTER) {
					   try {
						os.flush();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					}
				   }
			   
			  });
		
		
		JMenuItem mntmRunConfig = new JMenuItem("Run Configuration");
		mnRun.add(mntmRunConfig);
			mntmRunConfig.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
				try	{
					runConfiguration.setTitle("Run Configuration");
					runConfiguration.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					
					runConfiguration.setBounds(120, 120, 400, 400);
					runConfiguration.setModal(true);

					contentPane = new JPanel();
					contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
					runConfiguration.setContentPane(contentPane);
					contentPane.setLayout(null);
					JLabel config = new JLabel("Configuration Name");
					config.setBounds(10, 11, 140, 25);
					contentPane.add(config);
					configName = new JTextField();
					configName.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 1));
			        configName.setBounds(145, 11, 300, 25);
			        configName.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
						}
					});
			        contentPane.add(configName);
			        
			        
					JLabel packageName1 = new JLabel("Package Name");
					packageName1.setBounds(10, 46, 300, 25);
					contentPane.add(packageName1);
					pName = new JTextField();
					pName.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 1));
			        pName.setBounds(145, 46, 300, 25);
					pName.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							
						}
					});
					pName.setEditable(true);
					
					contentPane.add(pName);
					
					JLabel fileName = new JLabel("Main Class");
					fileName.setBounds(10, 81,300 , 25);
					contentPane.add(fileName);
					fName = new JTextField();
					fName.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 1));
			        fName.getText();
			        
					fName.setBounds(145, 81, 300,25 );
					contentPane.add(fName);
					
					JLabel arg = new JLabel("Arguments");
					
					arg.setBounds(10, 140,300 ,25);
					contentPane.add(arg);
				
					arguments = new JTextArea();
					arguments.setBounds(10, 141, 300, 45);
					arguments.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 1));
			        arguments.setColumns(75);
			        arguments.setRows(100);
			        arguments.setText(" ");
					
					JScrollPane scrollpanel = new JScrollPane(arguments);
				
					
					scrollpanel.createVerticalScrollBar();
					scrollpanel.createHorizontalScrollBar();
					scrollpanel.setBounds(165, 141, 250, 100);
					contentPane.add(scrollpanel);
										
					
					build = new JButton("SAVE");
					//Save a newly created run configuration
					build.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent arg0) {
							if( ( fName.getText() != null) && (pName.getText() != null ) && (configName.getText() != null) ){
								///Add to Config File
								String xmlPath = directory+"/run.cfg";
								SAXBuilder saxBuilder = new SAXBuilder();
								File xmlFile = new File(xmlPath);
								//Parse the XML tree and add the new entries to the bottom of the tree
								try{
									org.jdom2.Document doc = (org.jdom2.Document) saxBuilder.build(xmlFile);
									Element rootElement = doc.getRootElement();	
									
									Element newNode = new Element("item");
									newNode.setAttribute("config", configName.getText());
									Element addProj = new Element("Project").setText(projectName);
									newNode.addContent(addProj);
									Element addMain = new Element("Main").setText(fName.getText());
									newNode.addContent(addMain);
									Element addPkg = new Element("Package").setText(pName.getText());
									newNode.addContent(addPkg);
									Element addArgs = new Element("Args").setText(arguments.getText());
									newNode.addContent(addArgs);
									
									rootElement.addContent(newNode);
									
									rootElement.getChild("item");
									
									XMLOutputter xmlOutput = new XMLOutputter();
									 
									// display nice nice
									xmlOutput.setFormat(Format.getPrettyFormat());
									xmlOutput.output(doc, new FileWriter(directory+"/run.cfg"));
								}catch (IOException io) {
									System.out.println(io.getMessage());
								} catch (JDOMException jdomex) {
									System.out.println(jdomex.getMessage());
								}
								runConfiguration.setVisible(false);
							}
						}
					});
					
					
			        build.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
			        build.setPreferredSize(new java.awt.Dimension(100, 90));
			        build.setBounds(145, 281, 125, 25);
					contentPane.add(build);
					
					cancel = new JButton("CANCEL");
					cancel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
			        cancel.setPreferredSize(new java.awt.Dimension(100, 30));
			      
					cancel.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent arg0) {
						
							runConfiguration.setVisible(false);
						}
					});
					
					cancel.setBounds(280, 281, 125, 25);
					contentPane.add(cancel);
					runConfiguration.setLocationRelativeTo(null);
					runConfiguration.pack();
					runConfiguration.setResizable(false);
					runConfiguration.setSize(500, 400);
					runConfiguration.setVisible(true);
					
				}
				catch(Exception e10) {
					
				}
			}
			});
		/*
		 * GredBagLayout declaration
		 * DO NOT MOVE!
		 */
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		status = new JLabel(directory);
		GridBagConstraints gbc_filenameLabel = new GridBagConstraints();
		gbc_filenameLabel.anchor = GridBagConstraints.WEST;
		gbc_filenameLabel.insets = new Insets(0, 0, 5, 5);
		gbc_filenameLabel.gridx = 2;
		gbc_filenameLabel.gridy = 0;
		getContentPane().add(status, gbc_filenameLabel);
			
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridheight = 10;
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 1;
		getContentPane().add(treeFrame, gbc_panel);

		scrPane.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_filePane = new GridBagConstraints();
		gbc_filePane.gridwidth = 4;
		gbc_filePane.gridheight = 7;
		gbc_filePane.insets = new Insets(0, 0, 5, 0);
		gbc_filePane.fill = GridBagConstraints.BOTH;
		gbc_filePane.gridx = 2;
		gbc_filePane.gridy = 1;
		getContentPane().add(scrPane, gbc_filePane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
		gbc_tabbedPane.gridheight = 3;
		gbc_tabbedPane.gridwidth = 4;
		gbc_tabbedPane.fill = GridBagConstraints.BOTH;
		gbc_tabbedPane.gridx = 2;
		gbc_tabbedPane.gridy = 8;
		getContentPane().add(tabbedPane, gbc_tabbedPane);
		
		JScrollPane compilerScroll = new JScrollPane();
		tabbedPane.addTab("Compiler", null, compilerScroll, null);
		compilerScroll.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
		compilerScroll.setViewportView(compilerPane);
		
		compilerPane.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
	    
		compilerPane.setEditable(false);
		
		JScrollPane consoleScrll = new JScrollPane();
		tabbedPane.addTab("Console", null, consoleScrll, null);
		consoleScrll.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
		consoleScrll.setViewportView(consolePane);
		
		/*
		 * Window for Find Option
		 */
		findFrame.setTitle("Find/Replace");
		findFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		findFrame.setBounds(100, 100, 440, 160);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		findFrame.setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblFind = new JLabel("Find");
		lblFind.setBounds(10, 11, 40, 25);
		contentPane.add(lblFind);
		
		searchField = new JTextField();
		searchField.setBounds(94, 11, 180, 25);
		searchField.setColumns(30);
		
		contentPane.add(searchField);
		
		JLabel replaceLabel1 = new JLabel("Replace with");
		replaceLabel1.setBounds(10, 44, 75, 25);
		contentPane.add(replaceLabel1);
	
		replaceWith = new JTextField(25);
		replaceWith.setBounds(94, 44, 180, 25);
		replaceWith.setColumns(30);
		contentPane.add(replaceWith);
		
		final JCheckBox chckbxMatchCase = new JCheckBox("Match Case");
		chckbxMatchCase.setBounds(294, 12, 97, 23);
		contentPane.add(chckbxMatchCase);
		
		JButton btnFindNext = new JButton("Find Next");
		btnFindNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		
			    SearchContext context = new SearchContext();
			    
			    // will return the text in the find.
			    text = searchField.getText();
			    if (text.length() == 0) {
			       return;
			    }
			    context.setSearchFor(text);
			    context.setMatchCase(chckbxMatchCase.isSelected());
			    context.setSearchForward(true);
			    context.setWholeWord(false);

			    boolean found = (SearchEngine.find(codeEditor, context) ) != null;
			    if (!found) {
			       JOptionPane.showMessageDialog(null, "Text not found");
			    }
			}
		});
		btnFindNext.setBounds(10, 82, 89, 23);
		contentPane.add(btnFindNext);
		
		JButton btnFindPrevious = new JButton("Find Previous");
		btnFindPrevious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				// Create an object defining our search parameters.
			    SearchContext context = new SearchContext();
			    
			    // will return the text in the find textfield.
			    text = searchField.getText();
			    if (text.length() == 0) {
			       return;
			    }
			    context.setSearchFor(text);
			    context.setMatchCase(chckbxMatchCase.isSelected());
			    context.setSearchForward(false);
			    context.setWholeWord(false);

			    boolean found = (SearchEngine.find(codeEditor, context)) != null;
			    if (!found) {
			    	JOptionPane.showMessageDialog(null, "Text not found");
            	}
			}
		});
		
		btnFindPrevious.setBounds(109, 82, 110, 23);
		contentPane.add(btnFindPrevious);
		
		JButton replace = new JButton("Replace");
	    
		 replace.addActionListener(new ActionListener()	{
		            public void actionPerformed(ActionEvent e)	{
		               SearchContext context = new SearchContext();
		 			   
		               // will return the text in the find.
		 			   text = searchField.getText();
		 			   context.setSearchFor(text);
		 			   context.getWholeWord(); 
					   context.setMatchCase(chckbxMatchCase.isSelected());
					   context.setSearchForward(true);
					   context.setWholeWord(false);
		 			   
					   if (text == null || text.length()==0) {
					    	return;
					   }
					    boolean found = (SearchEngine.find(codeEditor, context)) != null;
					    
					    if(found)	{
					    	System.out.println(replaceWith.getText());
					    	System.out.println("Name  "+context.getReplaceWith());
					    	codeEditor.replaceSelection(replaceWith.getText());
					    	return;
					    }  
					    if (!found) {
					    	JOptionPane.showMessageDialog(null, "Text not replaced");
					    }
					 } 
		         });     	   
	               
		 	replace.setBounds(230, 82, 80, 23);
			contentPane.add(replace);
			 			
			JButton btnCancel = new JButton("Close");
				btnCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						findFrame.setVisible(false);
					}
				});
				btnCancel.setBounds(320, 82, 100, 23);
				contentPane.add(btnCancel);
				findFrame.setResizable(false);
		}
	
    /** compile your files by JavaCompiler */
 
    
    static String readFile(String path) throws IOException 
	{
    	byte[] encoded = Files.readAllBytes(Paths.get(path));
    	return new String(encoded, Charset.defaultCharset());
    }
    

    
    public void makeTree(){
    	
    	tree = new JTree(new FileSelectorModel(directory));
    	tree.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        
    	treeFrame.setViewportView(tree);
    	tree.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                FileNode selectedNode = (FileNode) tree.getLastSelectedPathComponent();
                status.setText(selectedNode.getAbsolutePath());
                if (selectedNode.isFile()) {
                	codeEditor.setText(null);
                    try {
                        BufferedReader br = new BufferedReader(new FileReader(selectedNode.getAbsolutePath()));
                        String line = "";
                        while ((line = br.readLine()) != null) {
                        	Document doc = codeEditor.getDocument();
                        	doc.insertString(doc.getLength(), line, null);
                        	doc.insertString(doc.getLength(), System.getProperty("line.separator"), null);
                        }
                        br.close();
                    } catch (Exception exc) {
                        exc.printStackTrace();
                    }
                }
            }
        });
        
        tree.setCellRenderer(new DefaultTreeCellRenderer() {
    
        	/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
            public Component getTreeCellRendererComponent(
                    JTree tree,
                    Object value,
                    boolean sel,
                    boolean expanded,
                    boolean leaf,
                    int row,
                    boolean hasFocus) {

                // Call parent rendering to keep the default behaviour
                super.getTreeCellRendererComponent(
                        tree, value, sel,
                        expanded, leaf, row,
                        hasFocus);

                // And now specific stuff
                File currentFile = (File) value;
                
                // If the current node is a directory, and if it has no child,
                // or if they are not accessible, change the icon.
                if (currentFile.isDirectory() && (currentFile.list() == null || currentFile.list().length == 0)) {
                    if (expanded) {
                        
                    	setIcon(openIcon);
                    } else {
                        setIcon(closedIcon);
                    }
                }

                return this;
            }
        });
     }
    
    
    
}